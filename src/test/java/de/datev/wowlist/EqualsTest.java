package de.datev.wowlist;

import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class EqualsTest {

    /**
     * HashCode/Equals muss bei Entities überschrieben werden.
     */
    @Test
    public void equalsTodo(){
        UUID uuid = UUID.randomUUID();

        Todo todo1 = new Todo();
        todo1.setId(uuid);

        Todo todo2 = new Todo();
        todo2.setId(uuid);

        assertFalse(todo1 == todo2);
        assertTrue(todo1.equals(todo2));
    }

}

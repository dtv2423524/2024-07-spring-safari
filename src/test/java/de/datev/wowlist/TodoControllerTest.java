package de.datev.wowlist;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.util.Base64Util;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.Instant;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    TodoService todosService;

    final String defaultDescription = "Tee kochen";
    final Instant defaultDueDate = Instant.parse("2024-12-22T10:00:00Z");

    @BeforeEach
    public void setup() {
        this.todosService.deleteAll();
    }

    @Test
    public void canGetAListOfTodos() throws Exception {
//      https://en.wikipedia.org/wiki/Fluent_interface
        mockMvc.perform(getRequest("/todos")).andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void canCreateATodo() throws Exception {
        var content = """
                {
                 "description": "Tee kochen",
                 "dueDate": "2024-12-22T10:00:00Z",
                 "category": "feierabend"
                 }
                 """;

        mockMvc.perform(postRequest("/todos", content))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").isNotEmpty())
                .andExpect(MockMvcResultMatchers.jsonPath("$.description").value("Tee kochen"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.done").value("false"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.category").value("feierabend"))
                .andExpect(MockMvcResultMatchers.jsonPath("$.dueDate").isNotEmpty());
    }

    @Test
    public void itCanGetASingleTodo() throws Exception {
        var responseContent = createTodo().andReturn().getResponse().getContentAsString();
        Todo createdTod = objectMapper.readValue(responseContent, Todo.class);


        mockMvc.perform(getRequest("/todos/" + createdTod.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(createdTod.getId().toString()))
                .andExpect(jsonPath("$.description").value(createdTod.getDescription()));
    }

    @Test
    public void itCanUpdateAnExistingTodo() throws Exception {
        String contentAsString = createTodo().andReturn().getResponse().getContentAsString();

        Todo todoDTO = objectMapper.readValue(contentAsString, Todo.class);

        todoDTO.setDescription("Bier brauen");

        mockMvc.perform(putRequest("/todos/" + todoDTO.getId(), objectMapper.writeValueAsString(todoDTO)))
                .andReturn().getResponse().getContentAsString();

        mockMvc.perform(getRequest("/todos/" + todoDTO.getId()))
                .andExpect(jsonPath("$.description").value("Bier brauen"));
    }

    @Test
    public void itCanDeleteAnExistingTodo() throws Exception {
        String contentAsString = createTodo().andReturn().getResponse().getContentAsString();
        Todo todoDTO = objectMapper.readValue(contentAsString, Todo.class);

        mockMvc.perform(deleteRequest("/todos/" + todoDTO.getId()))
                .andExpect(status().isNoContent());

        mockMvc.perform(getRequest("/todos"))
                .andExpect(jsonPath("$", hasSize(0)));
    }

    @Test
    public void itCanFindTodosByCategory() throws Exception {
        var request = new Todo("Kaffee kaufen", defaultDueDate);
        request.setCategory("alltag");
        createTodo(request);

        request = new Todo("Kaffee kaufen", defaultDueDate);
        request.setCategory("wichtig");
        createTodo(request);

        mockMvc.perform(getRequest("/todos/query").param("category", "wichtig"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].category").value("wichtig"));
    }

    @Test
    public void itCanCreateSubTasks() throws Exception {
        var request = new Todo("Kaffee trinken", defaultDueDate);
        request.setCategory("alltag");
        var response = createTodo(request);
        var responseContent = response.andReturn().getResponse().getContentAsString();

        Todo createdTodo = objectMapper.readValue(responseContent, Todo.class);

        var addSubtaskRequest = new Subtask(("Wasser kochen"));

        var postRequest = postRequest("/todos/" + createdTodo.getId() + "/subtasks", objectMapper.writeValueAsString(addSubtaskRequest));

        mockMvc.perform(postRequest).andExpect(status().isCreated());
        mockMvc.perform(getRequest("/todos/" + createdTodo.getId()))
                .andExpect(jsonPath("$.subtasks").isArray())
                .andExpect(jsonPath("$.subtasks", hasSize(1)));
    }

    @Test
    public void itCanCreateNotes() throws Exception {
        var request = new Todo("Kaffee trinken", defaultDueDate);
        request.setCategory("alltag");
        var response = createTodo(request);
        var responseContent = response.andReturn().getResponse().getContentAsString();

        Todo createdTodo = objectMapper.readValue(responseContent, Todo.class);

        var newNote = new Note(("Wasser kochen"));
        var postRequest = postRequest("/todos/" + createdTodo.getId() + "/notes", objectMapper.writeValueAsString(newNote));

        mockMvc.perform(postRequest).andExpect(status().isCreated());
        mockMvc.perform(getRequest("/todos/" + createdTodo.getId()))
                .andExpect(jsonPath("$.notes").isArray())
                .andExpect(jsonPath("$.notes", hasSize(1)));
    }

    /**
     * Erzeugt einen HTTP POST request - mit der notwendigen Authentifizierung.
     */
    private MockHttpServletRequestBuilder postRequest(String urlTemplate, String content) {
        return asDagobert(post(urlTemplate)
                .content(content)
                .contentType(MediaType.APPLICATION_JSON));
    }

    /**
     * Erzeugt einen HTTP GET request - mit der notwendigen Authentifizierung.
     */
    private MockHttpServletRequestBuilder getRequest(String urlTemplate) {
        return asDagobert(get(urlTemplate));
    }

    /**
     * Erzeugt einen HTTP DELETE request - mit der notwendigen Authentifizierung.
     */
    private MockHttpServletRequestBuilder deleteRequest(String urlTemplate) {
        return asDagobert(delete(urlTemplate));
    }

    /**
     * Erzeugt einen HTTP PUT request - mit der notwendigen Authentifizierung.
     */
    private MockHttpServletRequestBuilder putRequest(String urlTemplate, String content) {
        return asDagobert(put(urlTemplate).content(content).contentType(MediaType.APPLICATION_JSON));
    }

    private ResultActions createTodo(Todo request) throws Exception {
        return mockMvc.perform(postRequest("/todos", objectMapper.writeValueAsString(request)));
    }

    private ResultActions createTodo() throws Exception {
        var createTodoRequest = new Todo(defaultDescription, defaultDueDate);
        return this.createTodo(createTodoRequest);
    }

    /**
     * Fügt dem Request noch einen 'Authorization' header mit den credentials im Base46 encoding hinzu.
     *
     * So sieht der aus:
     *
     *  Authorization: Basic ZGFnb2JlcnQ6ZHVjaw==
     */
    private MockHttpServletRequestBuilder asDagobert(MockHttpServletRequestBuilder requestBuilder) {
        return requestBuilder.header(HttpHeaders.AUTHORIZATION, "Basic " + Base64Util.encode("dagobert:duck"));
    }

}

package de.datev.wowlist;

import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Optional;

public class OptionalTest {


    @Test
    public void test() {
        Optional<Todo> todo = helloWorld();

        Todo filledTodo = todo.orElse(new Todo("nix da", Instant.now()));
        System.out.println(filledTodo.getDescription());
    }

    public Optional<Todo> helloWorld(){
        return Optional.of(new Todo("kaffee trinken", Instant.now()));
    }

}

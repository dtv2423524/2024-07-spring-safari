package de.datev.wowlist;

import jakarta.validation.Validator;
import org.junit.jupiter.api.Test;

import java.time.Instant;

import static jakarta.validation.Validation.buildDefaultValidatorFactory;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class TodoTest {

    @Test
    public void itHasAnId() {
        var entry = new Todo();

        assertNotNull(entry.getId());
    }

    @Test
    public void descriptionCanNotBeEmpty() {
        try(var validatorFactory = buildDefaultValidatorFactory()){
            Validator validator = validatorFactory.getValidator();

            var violations = validator.validate(new Todo());

            assertEquals(1, violations.size());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void validIfXEqualsY() {
        try(var validatorFactory = buildDefaultValidatorFactory()){
            Validator validator = validatorFactory.getValidator();
            Todo todo = new Todo("Kaffe kochen", Instant.now().plusSeconds(60));
            todo.setX(42);
            todo.setY(42);

            var violations = validator.validate(todo);

            assertEquals(0, violations.size());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void invalidIfXNotEqualsY() {
        try(var validatorFactory = buildDefaultValidatorFactory()){
            Validator validator = validatorFactory.getValidator();
            Todo todo = new Todo("Kaffe kochen", Instant.now().plusSeconds(60));
            todo.setX(23);
            todo.setY(42);

            var violations = validator.validate(todo);

            assertEquals(1, violations.size());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}

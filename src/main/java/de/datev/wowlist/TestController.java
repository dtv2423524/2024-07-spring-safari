package de.datev.wowlist;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class TestController {

    Logger logger = LoggerFactory.getLogger(TestController.class);

    @GetMapping("/example")
    public void test1(){
        logger.info("hello1");
    }

    @GetMapping("/example/hello")
    public void test2(){
        logger.info("hello2");
    }

    @GetMapping("/example/{idx}")
    public void example(@PathVariable String idx){
        logger.info("Endpoint " + idx + " was called");
    }


    /**
     * String, Long, ....
     * void
     * Todo
     *
     *
     */
    @PostMapping("/echo")
    public ResponseEntity postWithBody(@RequestBody String content){
        logger.info("Received text: {}", content);

        if(content.equalsIgnoreCase("hello world")){
            return ResponseEntity.status(418).body("hello teapot");
        }

        return ResponseEntity.ok().body(content);
    }

    @PostMapping("/calculate/add")
    public ResponseEntity<String> validateCalculation(@RequestBody String calculation){
        try {
            int firstSummand = Integer.parseInt(calculation.split("=")[0].split("\\+")[0]);
            int secondSummand = Integer.parseInt(calculation.split("=")[0].split("\\+")[1]);
            int result = Integer.parseInt(calculation.split("=")[1]);
            if (firstSummand + secondSummand == result) {
                return ResponseEntity.ok().build();
            } else {
                return ResponseEntity.badRequest().build();
            }
        } catch (Exception ex) {
            return ResponseEntity.badRequest().build();
        }
    }
}


package de.datev.wowlist;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import jakarta.persistence.*;
import jakarta.validation.constraints.AssertTrue;
import jakarta.validation.constraints.Future;
import jakarta.validation.constraints.NotBlank;

import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Entity
public class Todo {

    @Id
    private UUID id;

    @Column
    @NotBlank
    private String description;

    @Column
    private String category;

    @Column
    private boolean done;

    @Column
    @Future
    private Instant dueDate;

    private int x;

    private int y;

    // Todo <-> Subtask (bidirektionale Beziehung)
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "todo")
    @JsonManagedReference
    //@JsonIgnoreProperties
    List<Subtask> subtasks;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "todo")
    @JsonManagedReference
    //@JsonIgnoreProperties
    List<Note> notes;

    public Todo() {
        this.id = UUID.randomUUID();
    }

    public Todo(String description, Instant dueDate) {
        this.id = UUID.randomUUID();
        this.description = description;
        this.done = false;
        this.dueDate = dueDate;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public Instant getDueDate() {
        return dueDate;
    }

    public void setDueDate(Instant dueDate) {
        this.dueDate = dueDate;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<Subtask> getSubtasks() {
        return subtasks;
    }

    public void setSubtasks(List<Subtask> subtasks) {
        this.subtasks = subtasks;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @AssertTrue(message = "Todo is not valid")
    private boolean isValid() {
        return x == y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Todo todo = (Todo) o;
        return Objects.equals(id, todo.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    public void addSubtask(String description) {
        Subtask subtask = new Subtask(description);
        subtasks.add(subtask);

        // Todo <-> Subtask
        subtask.setTodo(this);
    }

    public void addNote(String description) {
        Note note = new Note(description);
        notes.add(note);

        // Todo <-> Note
        note.setTodo(this);
    }
}

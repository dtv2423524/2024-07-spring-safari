package de.datev.wowlist;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController("/helloWorld")
public class HelloController {

    private final SayHelloBean helloBean;

    public HelloController(SayHelloBean helloBean) {
        this.helloBean = helloBean;
    }

    @GetMapping("/helloWorld")
    public String hello() {
        return helloBean.sayHello();
    }
}

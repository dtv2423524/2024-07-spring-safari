package de.datev.wowlist;

import jakarta.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
public class TodoController {

    private final TodoService todoService;

//  Constructor Based Injection - Die @Autowired Annotation ist optional.
//  Sprig erkennt trotzdem, dass eine Abhängigkeit zum TodoService besteht.
    public TodoController(TodoService todoService) {
        this.todoService = todoService;
    }

    @GetMapping("/todos")
    public List<Todo> getTodo(){
        return todoService.getTodos();
    }

    @GetMapping("/todos/{id}")
    public Todo getTodo(@PathVariable UUID id) {
        return this.todoService.getById(id);
    }

    @PostMapping("/todos")
    @ResponseStatus(HttpStatus.CREATED)
    public Todo createTodo(@RequestBody Todo todoToCreate){
        return this.todoService.createTodo(todoToCreate);
    }

    @PutMapping("/todos/{id}")
    public void updateTodo(@PathVariable UUID id, @RequestBody Todo request) {
        todoService.update(id, request.getDescription(), request.isDone(), request.getDueDate(), request.getCategory());
    }

    @DeleteMapping("/todos/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteTodo(@PathVariable UUID id) {
        this.todoService.delete(id);
    }

    // localhost:8080/todos/query?category=spaeter
    @GetMapping("/todos/query")
    public List<Todo> queryByParam(@RequestParam("category") String category){
        return todoService.findAllByCategory(category);
    }

    @PostMapping("/todos/{id}/subtasks")
    @ResponseStatus(HttpStatus.CREATED)
    public Todo addSubtask(@PathVariable UUID id, @RequestBody Subtask subtask){
        return this.todoService.addSubtask(id, subtask.getDescription());
    }

    @PostMapping("/todos/{id}/notes")
    @ResponseStatus(HttpStatus.CREATED)
    public Todo addSubtask(@PathVariable UUID id, @RequestBody Note note){
        return this.todoService.addNote(id, note.getDescription());
    }

}

package de.datev.wowlist;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface TodoRepository extends JpaRepository<Todo, UUID> {

    List<Todo> findAllByCategory(String category);

    /*

    @Query(value = "select todo from Todo todo where todo.description = :description", nativeQuery = false)
    List<Todo> findByDescriptionUsingJPQL(String description);

    @Query(value = "select * from TODO where description = :description", nativeQuery = true)
    List<Todo> findByDescriptionUsingSQL(String description);

    */
}

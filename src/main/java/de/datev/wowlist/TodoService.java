package de.datev.wowlist;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Service
public class TodoService {

    private TodoRepository todoRepository;

    public List<Todo> getTodos() {
        return todoRepository.findAll();
    }

    @Autowired
    TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }
    
    public Todo createTodo(Todo todoToCreate) {
        return todoRepository.save(todoToCreate);
    }

    public Todo update(UUID id, String description, boolean done, Instant dueDate, String category) {
        Todo todo = this.getById(id);
        todo.setDescription(description);
        todo.setDone(done);
        todo.setDueDate(dueDate);
        todo.setCategory(category);

        return todoRepository.save(todo);
    }

    public void delete(UUID id) {
        todoRepository.deleteById(id);
    }

    public void deleteAll(){
        todoRepository.deleteAll();
    }

    public Todo getById(UUID id) {
        return todoRepository.findById(id).orElseThrow();
    }

    public List<Todo> findAllByCategory(String category) {
        return todoRepository.findAllByCategory(category);
    }

    public Todo addSubtask(UUID todoId, String description) {
        Todo todo = this.getById(todoId);
        todo.addSubtask(description);

        return this.todoRepository.save(todo);
    }

    public Todo addNote(UUID id, String description) {
        Todo todo = this.getById(id);
        todo.addNote(description);

        return this.todoRepository.save(todo);
    }
}
